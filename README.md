# CONCEPTS DEV 23-24

## Objectif du Projet

Ce projet vise à créer une documentation collaborative sur les concepts avancés en Laravel et Vue.js. Chaque étudiant contribuera à cette documentation en expliquant un concept spécifique, en se basant sur la documentation officielle de Laravel.

## Comment Contribuer

1. Cloner le projet en local via GitKraken.
2. Ouvrez le projet dans VSCode.

### Choisir une Issue

1. Consultez la liste des issues disponibles dans le projet.
2. Sélectionnez une issue qui n'a pas encore été attribuée.
3. Attribuez-vous l'issue en l'assignant à votre nom d'utilisateur.

### Travailler sur l'Issue

1. Déplacez l'issue dans le board de projet au statut "Doing".
2. Clonez le projet en local sur votre machine en utilisant la commande :
3. Créez une nouvelle "branch" pour travailler sur votre issue.
4. Faites un Pull dans GitKraken.
5. Double-cliquez sur votre branche et vérifiez que VSCode est bien sur cette branche.
6. Vous êtes prêt.e à coder.

### Créer et Éditer un Document Markdown avec VSCode

#### Installation des Plugins

1. Ouvrez VSCode.
2. Allez dans la barre latérale et cliquez sur l'icône "Extensions" (le carré divisé en quatre).
3. Recherchez et installez les extensions suivantes :
   - **Markdown All in One** (par Yu Zhang) : pour des fonctionnalités de base comme les raccourcis clavier, la création de tables, et plus.
   - **Markdown Preview Enhanced** (par Yiyi Wang) : pour une prévisualisation améliorée de Markdown avec des fonctionnalités supplémentaires.

#### Création d'un Fichier Markdown

1. Dans VSCode, créez un nouveau fichier avec l'extension `.md` (par exemple, `concept.md`).

#### Rédaction en Markdown

1. Commencez à rédiger votre document en utilisant la syntaxe Markdown.
2. Utilisez les raccourcis clavier fournis par l'extension "Markdown All in One".

#### Utilisation de la Prévisualisation Markdown

1. Pour voir le rendu, utilisez la fonction de prévisualisation Markdown.
2. Cliquez sur l'icône "Aperçu Markdown" via un clic-droit.
3. La prévisualisation s'ouvrira sur le côté de votre document.

#### Enregistrement et Soumission

1. Enregistrez régulièrement votre travail.
2. Faites un commit à chaque étape importante et stable.
3. Poussez vos commits sur votre branche.

### Soumettre votre Contribution

1. Une fois votre travail terminé, faites votre dernier push et passez votre issue en "To Be Reviewed".
2. Créez un merge request à partir de votre branche et ajoutez-moi en tant que reviewer.
3. Ajoutez un commentaire dans la merge request expliquant les changements et liez l'issue correspondante.

### Après la Révision

1. Si des modifications sont demandées, effectuez-les et poussez les nouvelles modifications.
2. Une fois la merge request approuvée, elle sera fusionnée dans le dépôt principal.

## Règles de Contribution

- Assurez-vous que votre contribution est claire, concise, pratique et bien documentée.
- Respectez les conventions de codage et de documentation établies dans le projet.
- Vérifiez votre travail pour les fautes d'orthographe et de grammaire.

## Support

Pour toute question ou aide supplémentaire, n'hésitez pas à ouvrir une issue ou à me contacter directement.

## Statut du Projet

Ce projet est actuellement en cours de développement. Les contributions actives sont encouragées et appréciées.

---
