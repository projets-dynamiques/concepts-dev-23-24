
# Gestion des fichiers dans Laravel

[Lien vers la documentation officielle de la gestion des fichiers](https://laravel.com/docs/10.x/filesystem#main-content)

Ce guide fournit une introduction étape par étape à la gestion des fichiers dans Laravel, y compris la configuration, l'utilisation des différents drivers, et la personnalisation.

## 1. Introduction au Stockage de Fichiers dans Laravel
Laravel utilise un système de fichiers abstrait qui vous permet d'interagir de la même manière avec différents types de stockage. Cela signifie que vous pouvez changer de système de fichiers (local, Amazon S3, etc.) sans avoir à modifier le code de votre application.

## 2. Configuration des Systèmes de Fichiers
Votre configuration de système de fichiers se trouve dans `config/filesystems.php`. C'est là que vous définissez les "disques". Chaque disque représente un type de stockage et un emplacement spécifiques.

### Exemple de Configuration :
```php
'disks' => [
    'local' => [
        'driver' => 'local',
        'root' => storage_path('app'),
    ],
],
```
## 3. Le Driver Local
Lorsque vous utilisez le driver local, toutes les opérations de fichiers sont effectuées par rapport au répertoire racine défini dans votre fichier de configuration des systèmes de fichiers.Donc si on ce refere a l exemple ci-dessus le chemin sera `votres_dossier_projet/app`

### Exemple de Stockage de Fichier Localement :
```php
use Illuminate\Support\Facades\Storage;

Storage::disk('local')->put('example.txt', 'Contenu du fichier');
```

## 4. Le Disque Public
Le disque public est conçu pour les fichiers devant être accessibles publiquement. Vous créez un lien symbolique qui relie public/storage à storage/app/public pour rendre les fichiers accessibles via HTTP.

### Création d'un Lien Symbolique :
```bash
php artisan storage:link
```

## 5. Prérequis des Drivers
Pour utiliser des systèmes de stockage tels qu'Amazon S3, vous devez installer des packages supplémentaires via Composer.
(voir le .15 ou on developera tous cela avec dropbox)

### Installation du Package pour Amazon S3 :
```bash
composer require league/flysystem-aws-s3-v3 "^3.0"
```

## 6. Obtenir des Instances de Disque
Vous pouvez obtenir une instance de n'importe quel disque configuré en utilisant la façade Storage.

### Exemple :
```php
$disk = Storage::disk('s3');
```

## 7. Disques à la Demande
Vous pouvez configurer un disque "à la volée" si vous avez besoin de paramètres qui ne sont pas dans votre fichier de configuration.

### Exemple :
```php
$disk = Storage::build([
    'driver' => 'local',
    'root' => '/chemin/personnalisé',
]);
```

## 8. Récupération de Fichiers
Vous pouvez récupérer le contenu d'un fichier stocké sur un disque.

### Exemple :
```php
$contents = Storage::get('example.txt');
```

## 9. Stockage de Fichiers
Vous pouvez stocker des fichiers sur un disque en utilisant la méthode `put`.

### Exemple :

####en cas de stockage sur le disque par defaut
```php
Storage::put('example.txt', 'Contenu du fichier');
```
####En cas de stockage sur un disk spécifique
```php
Storage::disk('s3')->put('example.txt', 'Contenu du fichier');
```

## 10. Uploads de Fichiers
Laravel facilite le stockage des fichiers téléchargés par les utilisateurs grâce à la méthode `store`.

### Exemple d'Upload d'un fichier par un utilisateur :
```php
public function store(Request $request)
{
    $path = $request->file('avatar')->store('avatars');

    return $path; // Retourne le chemin où le fichier est stocké
}
```

## 11. Visibilité des Fichiers
Dans Laravel, vous pouvez définir la visibilité d'un fichier lors de son stockage pour déterminer s'il doit être accessible publiquement ou non.

### Exemple pour rendre un fichier public :
```php
Storage::put('example.txt', 'Contenu du fichier', 'public');
```

## 12. Suppression de Fichiers
Pour supprimer un fichier d'un disque, utilisez la méthode `delete`.

### Exemple de suppression d'un fichier :
```php
Storage::delete('example.txt');
```

## 13. Gestion des Répertoires
Vous pouvez lister, créer ou supprimer des répertoires sur vos disques.

### Exemple de création d'un répertoire :
```php
Storage::makeDirectory('nouveau_dossier');
```

### Exemple de suppression d'un répertoire :
```php
Storage::deleteDirectory('dossier_à_supprimer');
```
### Exemple de listage des répertoires :
```php
// Listage des répertoires dans le répertoire racine
$directories = Storage::directories();

// Listage de tous les répertoires, sous-repertoires,sous-sous-repertoires,.... dans le répertoire racine
$allDirectories = Storage::allDirectories();
```

## 14. Tests de Stockage de Fichiers
Avec Laravel, tester l'upload et le stockage de fichiers est simplifié grâce à la méthode `Storage::fake()`.

### Exemple de test d'upload de fichier :
```php
public function testFileUpload()
{
    Storage::fake('avatars');

    $response = $this->json('POST', '/upload', [
        'avatar' => UploadedFile::fake()->image('avatar.jpg')
    ]);

    // Assert the file was stored...
    Storage::disk('avatars')->assertExists('avatar.jpg');
}
```

## 15. Systèmes de Fichiers Personnalisés
Si vous avez besoin d'utiliser un système de stockage qui n'est pas inclus par défaut dans Laravel, vous pouvez créer votre propre "driver".

### Exemple d'enregistrement d'un nouveau driver (ici avec Dropbox) :
#### Étape 1 : Installer le Package Spatie Dropbox

1. Installez le package `spatie/flysystem-dropbox` via Composer :
   ```bash
   composer require spatie/flysystem-dropbox
   ```

#### Étape 2 : Créer une Application Dropbox et Obtenir un Token

2. Créez une application Dropbox sur [App Console de Dropbox](https://www.dropbox.com/developers/apps).
3. Configurez les autorisations pour inclure `files.content.write`.
4. Générez un jeton d'accès (token) pour votre application Dropbox.

#### Étape 3 : Configurer le Driver Dropbox dans Laravel

5. Ajoutez les informations de Dropbox à votre fichier `.env` :
   ```plaintext
   DROPBOX_TOKEN=votre_token_d'accès
   ```

6. Configurez le driver Dropbox dans `config/filesystems.php` :
   ```php
   'disks' => [
       // ...
       'dropbox' => [
           'driver' => 'dropbox',
           'authorization_token' => env('DROPBOX_TOKEN'),
       ],
       // ...
   ],
   ```

7. Étendez la façade Storage dans `App\Providers\AppServiceProvider.php` :
   ```php
   use Illuminate\Support\Facades\Storage;
   use League\Flysystem\Filesystem;
   use Spatie\FlysystemDropbox\DropboxAdapter;
   use Spatie\Dropbox\Client as DropboxClient;
   use Illuminate\Filesystem\FilesystemAdapter;

   public function boot()
   {
       Storage::extend('dropbox', function ($app, $config) {
           $client = new DropboxClient($config['authorization_token']);
           return new FilesystemAdapter(
               new Filesystem(new DropboxAdapter($client), $config)
           );
       });
   }
   ```

#### Étape 4 : Créer un Contrôleur pour Gérer la Création de Fichiers

8. Créez un contrôleur pour gérer la création de fichiers :
   ```bash
   php artisan make:controller DropboxController
   ```

9. Ajoutez une méthode dans le contrôleur pour créer un fichier dans Dropbox (`app/Http/Controllers/DropboxController.php`) :
   ```php
   namespace App\Http\Controllers;
   use Illuminate\Support\Facades\Storage;

   class DropboxController extends Controller
   {
       public function createFile()
       {
           Storage::disk('dropbox')->put('hello.txt', 'Coucou');
           return back()->with('status', 'Fichier créé avec succès dans Dropbox.');
       }
   }
   ```

#### Étape 5 : Définir la Route

10. Définissez une route pour accéder à cette fonctionnalité dans `routes/web.php` :
    ```php
    use App\Http\Controllers\DropboxController;

    Route::get('/create-file', [DropboxController::class, 'createFile']);
    ```

#### Étape 6 : Tester la Fonctionnalité

11. Accédez à l'URL `/create-file` dans votre navigateur ou utilisez un bouton dans une vue pour déclencher cette route.
