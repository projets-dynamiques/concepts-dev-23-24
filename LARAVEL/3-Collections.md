
# Éloquent : Collections

## Introduction

Dans Laravel, les collections Eloquent enrichissent la manipulation des ensembles de tableaux indexé retournés par Eloquent. Ces collections offrent des méthodes intuitive pour filtrer, trier, et transformer les données facilement.

## Parcourir un tableau

```php
use App\Models\User;

$users = User::where('active', 1)->get();

foreach ($users as $user) {
    echo $user->name;
}
```

## Opérations Courantes

### Filtrer les tableaux

```php
$activeUsers = User::all()->reject(function ($user) {
    return !$user->active;
});
```

### Transformer les tableaux

```php
$names = User::all()->map(function ($user) {
    return $user->name;
});
```

### Supprimer les modèles inactifs et collecter les noms 

```php
$names = User::all()->reject(function (User $user) {
    return $user->active === false;
})->map(function (User $user) {
    return $user->name;
});
```

## Méthodes de Collection Importantes

- **`all`** : Retourne tous les éléments de la collection.
- **`pluck`** : Extraire des valeurs spécifiques.
- **`where`** : Filtrer la collection par une clé/valeur.
- **`sortBy`** : Trier la collection.

### Exemples de Méthodes

- **`append`** : Ajouter un attribut à chaque modèle de la collection.

```php
$users->append('team');
```

- **`contains`** : Vérifier si une collection contient un modèle.

```php
$users->contains(1);
```

- **`find`** : Trouver un modèle par sa clé primaire.

```php
$user = $users->find(1);
```

- **`load`** : Charger avec impatience les relations pour tous les modèles de la collection.

```php
$users->load(['comments', 'posts']);
```

- **`unique`** : Renvoyer tous les modèles uniques de la collection.

```php
$users = $users->unique();
```

## Collections Personnalisées

Pour étendre les fonctionnalités des collections Eloquent, vous pouvez définir une collection personnalisée.

### Définir une Collection Personnalisée

```php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Collections\CustomUserCollection;

class User extends Model
{
    public function newCollection(array $models = [])
    {
        return new CustomUserCollection($models);
    }
}
```

## Utilisation

Avec une collection personnalisée, vous pouvez ajouter des méthodes spécifiques pour manipuler vos données.

## Conclusion

Les collections Eloquent offrent une interface puissante pour manipuler des ensembles de données, et la possibilité de créer des collections personnalisées ouvre la voie à une manipulation de données encore plus flexible.
