
# Guide Complet sur les Migrations, Factories et Seeders dans Laravel

## Sommaire
- [Migrations dans Laravel](#migrations-dans-laravel)
- [Factories et Seeders dans Laravel](#factories-et-seeders-dans-laravel)

## Migrations dans Laravel
[Lien vers la documentation officielle des migrations](https://laravel.com/docs/10.x/migrations)

### Introduction
Les migrations sont essentielles pour définir et modifier la structure de la base de données de manière suivie et programmée dans Laravel.

### Création de Tables
Les migrations permettent de créer de nouvelles tables dans la base de données.

#### Exemple : Table `flights`
```php
Schema::create('flights', function (Blueprint $table) {
    $table->id();
    $table->string('name');
    $table->string('airline');
    $table->integer('flightNumber');
    $table->timestamps();
});
```
Cette migration crée une table `flights` avec des colonnes pour l'identifiant, le nom, la compagnie aérienne, et un numéro de vol.

### Ajout de Colonnes
Vous pouvez ajouter des colonnes à une table existante en créant une nouvelle migration.

#### Exemple : Ajout de `destination`
```php
Schema::table('flights', function (Blueprint $table) {
    $table->string('destination');
});
```
Cette ligne ajoute une colonne `destination` à la table `flights`.

### Relations dans les Migrations
Les migrations peuvent également être utilisées pour définir des relations entre les tables.

#### Relation 1:n (Un à Plusieurs)
Supposons qu'un vol (`flight`) peut avoir plusieurs passagers (`passengers`).

##### Table `passengers`
```php
Schema::create('passengers', function (Blueprint $table) {
    $table->id();
    $table->string('name');
    $table->foreignId('flight_id')->constrained();
    $table->timestamps();
});
```
Ici, `foreignId('flight_id')` crée une clé étrangère qui fait référence à la table `flights`, établissant une relation 1:n.

#### Relation n:m (Plusieurs à Plusieurs)
Pour une relation n:m, par exemple entre `flights` et `pilots`, une table intermédiaire est nécessaire.

##### Table Pivot `flight_pilot`
```php
Schema::create('flight_pilot', function (Blueprint $table) {
    $table->id();
    $table->foreignId('flight_id')->constrained();
    $table->foreignId('pilot_id')->constrained();
    $table->timestamps();
});
```
Cette table sert de table pivot pour la relation n:m, contenant des clés étrangères pour `flights` et `pilots`.

### Exécution des Migrations
Pour appliquer les migrations :
```bash
php artisan migrate
```
Pour annuler la dernière migration :
```bash
php artisan migrate:rollback
```


## Factories et Seeders dans Laravel
[Lien vers la documentation officielle des factories](https://laravel.com/docs/10.x/eloquent-factories#main-content)
[Lien vers la documentation officielle des seeders](https://laravel.com/docs/10.x/seeding)

### Introduction
Dans Laravel, les `factories` et les `seeders` sont utilisés ensemble pour générer et insérer des données de test dans la base de données. Les factories définissent comment les données sont générées, tandis que les seeders exécutent l'insertion des données.

### Partie 1: Factories
Les factories dans Laravel sont des classes qui définissent comment créer des instances de modèles avec des données fictives mais plausibles.

#### Création d'une Factory pour `Flight`
Par exemple, pour créer des données de test pour un modèle `Flight` :

1. **Génération d'une Factory**
   Utilisez la commande Artisan :
   ```bash
   php artisan make:factory FlightFactory --model=Flight
   ```

2. **Définition des Données Fictives**
   Dans la factory, utilisez la bibliothèque Faker pour définir les données fictives :
   ```php
   use Faker\Generator as Faker;

   class FlightFactory extends Factory {
       protected $model = Flight::class;

       public function definition() {
           return [
               'name' => $this->faker->word,
               'airline' => $this->faker->company,
               'flightNumber' => $this->faker->randomNumber(),
               'destination' => $this->faker->city
           ];
       }
   }
   ```

### Partie 2: Seeders
Les seeders sont utilisés pour alimenter la base de données avec les données définies par les factories.

#### Création d'un Seeder pour `flights`
Pour insérer des données dans la table `flights` :

1. **Génération d'un Seeder**
   ```bash
   php artisan make:seeder FlightsTableSeeder
   ```

2. **Utilisation de la Factory dans le Seeder**
   Dans le seeder, utilisez la factory pour créer des données :
   ```php
   use Illuminate\Database\Seeder;

   class FlightsTableSeeder extends Seeder {
       public function run() {
           Flight::factory()->count(20)->create();
       }
   }
   ```

3. **Exécution du Seeder**
   Pour peupler la base de données :
   ```bash
   php artisan db:seed --class=FlightsTableSeeder
   ```

### Bonnes Pratiques
- Utilisez les factories pour définir des modèles de données réalistes et variées.
- Les seeders doivent être utilisés avec prudence, particulièrement dans les environnements de production.
- Testez vos seeders dans un environnement de développement pour vous assurer qu'ils fonctionnent comme prévu..