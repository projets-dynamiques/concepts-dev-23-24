#**Guide sur les concept de sérialisation et de Ressources API dans Laravel**

##**Sommaire**

- [Serialization](#serialization)
  - [1. Introduction](#1-introduction)
  - [2. Pourquoi sérialiser ?](#2-pourquoi-sérialiser-)
  - [3. Sérialisation d'un modèle](#3-sérialisation-dun-modèle)
- [Ressources API](#ressources-api)
  - [1. Introduction](#1-introduction-1)
  - [2. Pourquoi utiliser les Ressources API ?](#2-pourquoi-utiliser-les-ressources-api-)
  - [3. Vue d'ensemble du concept](#3-vue-densemble-du-concept)
  - [4. Création et rédaction de ressources](#4-création-et-rédaction-de-ressources)
    - [Relations](#relations)
  - [5. Création et rédaction de Collections de Ressources](#5-création-et-rédaction-de-collections-de-ressources)
  - [6. Gestion avancée des ressources dans Laravel](#6-gestion-avancée-des-ressources-dans-laravel)
    - [Encapsulation des données](#encapsulation-des-données)
    - [Pagination](#pagination)
    - [Attributs conditionnels](#attributs-conditionnels)
    - [Chargement anticipé des données liées (relations conditionnelles)](#chargement-anticipé-des-données-liées-relations-conditionnelles)
    - [Autorisations](#autorisations)
    - [Ajout de Métadonnées](#ajout-de-métadonnées)
    - [Réponses de Ressource](#réponses-de-ressource)
- [Sources](#sources)


## Serialization

### 1. Introduction
La sérialisation est un **processus général de conversion d'objets** en un format facilement stockable et transmissible (comme JSON), automatiquement géré par Laravel pour les modèles Eloquent. 

![Alt text](schema_serialization.png)

### 2. Pourquoi sérialiser ?

- **Stockage :** Facilite le stockage des objets dans des formats adaptés à la base de données.
- **Transmission :** Permet une transmission efficace des données entre le backend et le frontend.
- **Reconstruction :** Permet de reconstruire les objets Eloquent à partir de formats sérialisés.

### 3. Sérialisation d'un modèle

La sérialisation automatique d'un modèle en JSON se fait par le biais de la méthode **`toJson()`**:

```php
use App\Models\User;

$user = User::find(1);

return $user->toJson();
```
Grâce à cette méthode, nous pouvons maintenant décider :
- de cacher des attributs en définissant une propriété **`$hidden`** :

    ```php
    class User extends Model
    {
        /**
        * The attributes that should be hidden for arrays.
        *
        * @var array
        */
        protected $hidden = ['email', 'password'];
    }
    ```

- d'ajouter des attributs qui n'ont pas de colonne correspondante dans votre base de données à l'aide de la propriété **`$appends`**:

    ```php
    class User extends Model
    {
        protected $appends = ['is_admin'];
    }
    ```

- de personnaliser le format des dates par défaut en remplaçant la méthode serializeDate :

    ```php
    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d');
    }
    ```

    Il est aussi possible de personnaliser le format de date pour des attributs individuels dans les déclarations de cast du modèle :

    ```php
    protected $casts = [
        'birthday' => 'date:Y-m-d',
        'joined_at' => 'datetime:Y-m-d H:00',
    ];
    ```

Bien que très utile pour la conversion des données, la sérialisation automatique présente des limitations en termes de contrôle.

Afin d'aller au-delà de cette sérialisation automatique et de renforcer le contrôle sur l'affichage des données via l'API, Laravel nous propose des outils de sérialisation intégrés : **les Ressources API**.

## Ressources API
### 1. Introduction

**Les Ressources API** sont un type de classe particulier fourni par Laravel pour personnaliser la sérialisation des modèles en JSON.

Ces Ressources agissent comme **un proxy sur les propriétés de nos modèles**, nous permettant ainsi de définir clairement les champs que nous souhaitons exposer ou cacher au niveau de l'API.

### 2. Pourquoi utiliser les Ressources API ?

**1. Contrôle de la présentation des données :** permettent de mieux contrôler l'apparence des données dans les réponses JSON de l'API.
**2. Protection des données sensibles :** offrent une meilleure protection des données sensibles en permettant de limiter leur inclusion dans les réponses JSON de l'API.
**3. Réutilisabilité et évolutivité :** sont plus faciles à réutiliser et à adapter aux évolutions de l'API, ce qui rend le code plus flexible et évolutif.
**4. Facilité de documentation :** simplifient la documentation des endpoints de l'API en spécifiant clairement la structure des réponses JSON.

### 3. Vue d'ensemble du concept

Les ressources dans Laravel sont utilisées pour transformer un modèle donné en un tableau (array) convivial pour une API. Chaque ressource contient une méthode **`toArray()`** qui traduit les attributs de notre modèle en un tableau pouvant être renvoyé depuis les routes ou les contrôleurs de notre application ainsi qu'une variable **`$this`** qui permet d'accéder à toutes les propriétés du modèles.

**En résumé :**
![alt text](schema_api_resource.png)

### 4. Création et rédaction de ressources

Pour créer une ressource, utilisez la commande :
```bash
php artisan make:resource UserResource
```

Exemple de ressource :

```php
namespace App\Http\Resources;
 
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
 
class UserResource extends JsonResource
{
    /**
     * Transforme la ressource en un tableau.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            // Nous n'incluons pas l'e-mail pour des raisons de sécurité
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
```

Une fois définie, cette ressource peut être retournée directement depuis une route ou un contrôleur :

```php
use App\Http\Resources\UserResource;
use App\Models\User;

Route::get('/user/{id}', function (string $id) {
    return new UserResource(User::findOrFail($id));
});

```

#### Relations
Si vous souhaitez inclure des ressources liées dans votre réponse, vous pouvez les ajouter au tableau renvoyé par la méthode toArray de votre ressource. Par exemple, si vous voulez inclure les articles de blog de l'utilisateur dans la réponse :

```php
use App\Http\Resources\PostResource;
use Illuminate\Http\Request;

public function toArray(Request $request): array
{
    return [
        'id' => $this->id,
        'name' => $this->name,
        'posts' => PostResource::collection($this->posts),
        'created_at' => $this->created_at,
        'updated_at' => $this->updated_at,
    ];
}

```

### 5. Création et rédaction de Collections de Ressources

Il est également possible de générer des ressources pour transformer des collections de modèles grâce à 2 méthodes :
```bash
php artisan make:resource User --collection

php artisan make:resource UserCollection
```

Les ressources sont conçues pour manipuler individuellement un enregistrement spécifique, tandis que les collections de ressources sont destinées à traiter un ensemble d'enregistrements :

![alt text](resource_collection.png)

 Lorsque vous retournez une collection de ressources ou une réponse paginée dans votre application Laravel, il est recommandé d'utiliser la méthode collection fournie par la classe de ressource pour créer l'instance de ressource dans votre route ou votre contrôleur.

```php
use App\Http\Resources\UserResource;
use App\Models\User;

Route::get('/users', function () {
    return UserResource::collection(User::all());
});
```

Si vous avez besoin de personnaliser les métadonnées renvoyées avec la collection, il est nécessaire de les définir avec votre propre collection de ressources :

```php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    public function toArray(Request $request): array
    {
        return [
            'data' => $this->collection,
            'links' => [
                'self' => 'link-value',
            ],
            'meta' => [
                'total_users' => $this->collection->count(),
            ],
        ];
    }
}

```

Vous pouvez ensuite retourner cette collection de ressources personnalisée depuis une route ou un contrôleur :

```php
use App\Http\Resources\UserCollection;
use App\Models\User;

Route::get('/users', function () {
    return new UserCollection(User::all());
});

```

### 6. Gestion avancée des ressources dans Laravel
#### Encapsulation des données
Par défaut, Laravel enveloppe la réponse de notre ressource avec la clé 'data' lors de sa conversion en JSON. 

![alt text](data.png)

Vous pouvez personnaliser cette clé en définissant l'attribut $wrap dans votre classe de ressource :

```php
class UserResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string|null
     */
    public static $wrap = 'user';
}
```

#### Pagination
Vous pouvez passer une instance de paginage Laravel à la méthode collection d'une ressource ou à une collection de ressources personnalisée :

```php
use App\Http\Resources\UserCollection;
use App\Models\User;

Route::get('/users', function () {
    return new UserCollection(User::paginate(10));
});
```
Les réponses paginées incluent toujours les clés meta et links fournissant des informations sur l'état de la pagination.

![alt text](meta_data.png)

#### Attributs conditionnels
Les ressources API nous permettent également d'inclure des attributs dans une réponse uniquement si une condition donnée est remplie en utilisant la méthode `when()`:
```php
return [
    'id' => $this->id,
    'name' => $this->name,
    'secret' => $this->when($request->user()->isAdmin(), 'secret-value'),
];
```

Dans cet exemple, l'attribut `'secret'` sera retourné seulement si la méthode `isAdmin` user authentifié renvoie `true`.

#### Chargement anticipé des données liées (relations conditionnelles)
Pour optimiser les performances de votre API, vous pouvez charger anticipativement les données liées pour réduire le nombre de requêtes à la base de données. Par exemple :

```php
// Fetch users with their roles
$users = User::with('roles')->get();

// In the API Resource
public function toArray($request)
{
    return [
        'id' => $this->id,
        'name' => $this->name,
        'roles' => new RoleResource($this->whenLoaded('roles')),
    ];
}
```

#### Autorisations
Sécurisez votre API en implémentant des vérifications d'autorisation dans vos classes de ressources API pour contrôler l'accès à des ressources spécifiques. Par exemple :

```php
class UserResource extends JsonResource
{
    public function toArray($request)
    {
        if ($this->authorizedToView()) {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'pseudo' => $this->pseudo,
                // Ajoutez d'autres champs si nécessaire
            ];
        }
    }

    private function authorizedToView()
    {
        // Ajoutez votre logique d'autorisation ici
        return true;
    }
}
```
Cela garantit que seuls les utilisateurs autorisés peuvent accéder aux données sensibles via l'API.

#### Ajout de Métadonnées
Pour ajouter des métadonnées à votre réponse de ressource, incluez-les dans la méthode toArray de votre ressource :
```php
return [
    'data' => $this->collection,
    'links' => [
        'self' => 'link-value',
    ],
];
```

#### Réponses de Ressource
Les ressources peuvent être retournées directement depuis les routes et les contrôleurs. Pour personnaliser une réponse HTTP sortante, utilisez la méthode **`response`** ou définissez **`withResponse`** dans votre ressource :

```php
return (new UserResource(User::find(1)))
            ->response()
            ->header('X-Value', 'True');

```
---
## Sources
- https://laravel.com/docs/10.x/eloquent-resources
- https://laravel.com/docs/10.x/eloquent-serialization
- https://grafikart.fr/tutoriels/laravel-resource-api-2142
- https://arjunamrutiya.medium.com/a-comprehensive-guide-to-laravel-api-resources-8ee13aa289fb

