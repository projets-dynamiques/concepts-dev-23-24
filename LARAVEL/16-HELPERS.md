# 16. HELPERS

## Introduction 

Laravel inclut une variété de fonctions PHP globales, les "helpers". Elles sont utilisées par le framework lui-même, mais on peut les intégrer dans nos applications si nécessaire. 
C'est une série de fonctions, qui comme une boite à outils pratiques, permettant de traiter certains types de données.

*Documentation officielle des [Helpers](https://laravel.com/docs/10.x/helpers)*

## Exemples des différentes méthodes disponibles

###### 1. Chaînes de caractères
- `str_limit()` : limite une chaîne de caractères.
- `str_contains()` : vérifie si une sous-chaîne existe.
- `str_plural()` : retourne la forme au plurielle d'un mot.   

###### 2. Arrays & objects
Utilise la classe `Arr` fournit des méthodes statiques pour manipuler des tableaux.

- `Arr::add()` : ajoute une paire key-valeurau tableau. 
- `Arr::get()` : récupère la valeur d'une key. 
- `head()` : retourne le premier élément du tableau.
- `data_get()` : retourne la valeur d'un objet ou d'un tableau.

###### 3. Numbers
Utilise la classe `Number` fournit des méthodes statiques pour manipuler des tableaux.

- `Number::abbreviate()` : retourne le format lisible par l'homme de la valeur numérique fournie, avec une abréviation pour les unités.
- `Number::percentage()` : retourne en pourcentage la valeur donnée comme un *string*. 

###### 4. Paths
Fournit des chemins de fichiers absolus.

- `base_path()` : retourne le chemin de la racine de l'application.
- `storage_path()` : retourne le chemin du répertoire de stockage.
- `public_path()` : retourne le chemin du répertoire public.


###### 5. URLs 
Permet de générer automatiquement les URLs.

- `url()` : génère une URL absolue.
- `asset()` : génère l'URL vers un fichier asset.
- `route()` : génère l'URL pour une route nommée.

###### 6. Divers

- `auth()` : retourne une instance d'authentification.
- `blank()` : détermine si la valeur donnée est "vide".
- `dd()` : "dump&die, récupère les variables et met fin à l'exécutation du script.  

## Autres utilisations

###### 1. Benchmarking

Permet de tester rapidement les perfomances de certaines parties de votre application. 

Il faut pour cela utiliser la classe `Benchmark` 
=> `use Illuminate\Support\Benchmark;`

*Exemple* : 
```php 
Benchmark::dd(fn () => User::find(1)); // 0.1 ms
```

###### 2. Dates
Laravel intègre Carbon, une bibliothèque puissante pour la manipulation des dates et heures.

*Exemple* : Pour retourner l'heure et la date actuelle 

`$now = now()`  *(retourne directement une instance `Carbon`)*

On peut aussi appeler explicitement la classe `Carbon` : 
```php   
use Illuminate\Support\Carbon;

$now = Carbon::now();
```

*Documentation officielle de [Carbon](https://carbon.nesbot.com/docs/)*

###### 3. Lottery (loterie)
La classe Lottery est utilisée pour exécuter des actions basées sur des probabilités.

*Exemple* :
```php
use Illuminate\Support\Lottery;

Lottery::odds(1, 20)
   ->winner(fn () => $user->won())
   ->loser(fn () => $user->lost())
   ->choose();
```

Cette classe peut se combiner avec d'autres fonctionnalités que Laravel propose mais également avec des méthodes simples définies au préalable. 

###### 4. Pipeline
Ce sont des flux de données dans lesquels chaque élément est traité successivement par des helpers. Chaque fonction dans le pipeline effectue une action spécifique sur les données et les passe ensuite à la fonction suivante.

Ce sont des mécanismes similaires aux middlewares, mais ils ont des objectifs et des fonctionnalités légèrement différents.
  
- *Middleware* : c'est une couche intermédiaire entre la requête entrante et la réponse sortante d'une application. Il intercepte la requête HTTP et peut effectuer des actions telles que la validation, l'authentification, la manipulation de la requête, etc. avant de passer la main au gestionnaire de requête suivant.

Les **middlewares** sont utilisés principalement pour le traitement des requêtes HTTP, en effectuant des actions avant ou après que la requête soit traitée par le routeur de l'application.
Les **pipelines** sont plus adaptés au traitement en masse de collections de données, où une série d'actions doit être appliquée à chaque élément de la collection.

  - *Par exemple* :  si vous avez une collection d'objets et que vous devez appliquer une série de transformations à chacun d'eux, un pipeline serait un bon choix. Les pipelines sont généralement utilisés pour le traitement de données en masse plutôt que pour la gestion des requêtes HTTP.

Bien que les *pipelines* et les *middlewares* aient des utilisations similaires, ils sont conçus pour des tâches différentes. Les *middlewares* sont adaptés au traitement des requêtes HTTP, tandis que les *pipelines* sont utilisés pour traiter des collections de données à travers une série d'actions.

**Exemple** : 

Cet exemple utilise un pipeline pour traiter un utilisateur $user en passant par une série d'actions spécifiées par les classes
```php
$user = Pipeline::send($user)
         ->through([
               GenerateProfilePhoto::class, //génére une photo de       profil pour l'utilisateur, si il n'en a pas
               ActivateSubscription::class, //active l'abonnement de       l'utilisateur si il est inscrit à celui-ci
               SendWelcomeEmail::class, //peut envoyer un e-mail de     bienvenue à l'utilisateur.
                  ])
         ->then(fn (User $user) => $user);
```

Une fois que l'utilisateur a traversé toutes les étapes du pipeline, la fonction de rappel ***then*** est appelée avec l'utilisateur traité en tant qu'argument. Dans cet exemple, la fonction de rappel est une fonction fléchée (lambda) qui retourne simplement l'utilisateur tel quel.

*Documentation officielle des [Middlewares](https://laravel.com/docs/10.x/middleware)*

###### 5. Sleep

La classe Sleep est une enveloppe légère autour des fonctions natives sleep et usleep de PHP, offrant une meilleure testabilité tout en exposant une API conviviale pour les développeurs pour travailler avec le temps :
```php
use Illuminate\Support\Sleep;
      
$waiting = true;
      
while ($waiting) {
   Sleep::for(1)->second();

   $waiting = /* ... */;
}
```

La classe Sleep offre une variété de méthodes qui vous permettent de travailler avec différentes unités de temps.

```php
// Pause execution for 90 seconds...
Sleep::for(1.5)->minutes();

// Pause execution for 2 seconds...
Sleep::for(2)->seconds();
```

Lorsque vous testez du code utilisant la classe Sleep ou les fonctions de sommeil natives de PHP, vos tests sont ralentis car l'exécution est mise en pause.
Elle permet de simuler le sommeil dans les tests, contournant ainsi la pause réelle de l'exécution et accélérant les tests.