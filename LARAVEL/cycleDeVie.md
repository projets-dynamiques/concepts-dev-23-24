# Concept de Cycle de Vie en Laravel

 Le concept de cycle de vie en Laravel fait référence aux différentes étapes par lesquelles passe une requête HTTP lorsqu'elle est traitée par une application Laravel.

Voici une représentation schématique des étapes du cycle de vie d'une requête dans Laravel.

![Request Life Cycle in Laravel](images/CycledeVie.png){width=90%}

## 1. **Requête HTTP (Request)** <a name="Request"></a>
 
 Lorsqu'une requête HTTP arrive sur votre application Laravel, elle est traitée par le serveur web, tel qu'Apache ou Nginx, qui la transmet à PHP. Laravel reçoit ensuite la requête encapsulée dans un objet de requête (Request), contenant toutes les informations pertinentes. Cette étape se produit au niveau du serveur web et des fichiers de démarrage de Laravel, tels que `public/index.php`.

## 2. **Route (Routing)** <a name="Route"></a>
 
 Le système de routage de Laravel, défini dans les fichiers de routes tels que `routes/web.php` ou `routes/api.php`, correspond à la requête entrante à une action spécifique de votre application. Laravel utilise des expressions régulières et des méthodes expressives pour identifier la route appropriée à suivre.

## 3. **Middleware** <a name="Middleware"></a>
 
 Les middlewares sont des couches intermédiaires qui agissent avant et après le traitement de la requête. Définis dans `app/Http/Kernel.php` ou directement dans les fichiers de route, ils permettent d'effectuer des opérations telles que l'authentification, la validation des données, la gestion des sessions, etc.

## 4. **Contrôleur (Controller)** <a name="Controller"></a>

 Une fois que la requête a passé les middlewares, elle est transmise à la fonction de rappel du contrôleur associé à la route. Les contrôleurs sont des classes qui regroupent la logique de gestion des requêtes liées à un certain contexte de l'application.

## 5. **Logique d'application** <a name="Logique"></a>

 À ce stade, dans le contrôleur ou dans d'autres classes invoquées par le contrôleur, la logique métier de votre application est exécutée. Cela peut inclure l'accès à la base de données, le traitement de données, l'interaction avec des services externes, etc.

## 6. **Réponse (Response)** <a name="Response"></a>

 Une fois la logique d'application exécutée, le contrôleur retourne une réponse à la requête. Cette réponse peut être une vue HTML, des données JSON, un fichier téléchargé, etc. Les contrôleurs retournent souvent des réponses en utilisant les classes `Response` ou `View`.

## 7. **Middlewares de réponse** <a name="Middlewares_Response"></a>

La réponse générée par le contrôleur peut passer par des middlewares de réponse, définis dans `app/Http/Kernel.php` ou directement dans les fichiers de route. Ces middlewares peuvent modifier la réponse avant qu'elle ne soit renvoyée au client.

## 8. **Renvoi de la réponse HTTP (HTTP Response)** <a name="HTTP_Response"></a>

Enfin, la réponse est renvoyée au client qui a initialement fait la requête, généralement sous forme de document HTML, de données JSON, d'une image, etc. Cette réponse est envoyée par le serveur web, marquant la fin du cycle de vie de la requête.

> **En résumé**, *le cycle de vie en Laravel décrit le parcours qu'effectue une requête HTTP au sein de l'application*, depuis son entrée jusqu'à sa sortie. Ce cycle, orchestré par Laravel, offre une structure bien définie pour traiter les requêtes et les réponses, tout en permettant une grande souplesse dans la gestion de la logique métier grâce à l'utilisation de contrôleurs, de middlewares et de routes.

**Sources:**

- [Hitesh Kumar on Medium](https://medium.com/@hiteshkr8750/request-life-cycle-of-laravel-eeff4311e5ba)
- [Laravel Documentation](https://laravel.com/docs/10.x/lifecycle)
- [IONOS](https://www.ionos.fr/digitalguide/serveur/know-how/middleware/)
- [ChatGPT](https://chat.openai.com)
