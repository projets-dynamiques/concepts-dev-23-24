# Installer VueJS et Vite sur Laravel

### Setup de l'environnement
#### Créer son projet Laravel

* Aller dans le dossier racine voulu
* Ouvrir un terminal
* Utiliser la commande de création de projet laravel
  
``composer create-project laravel/laravel projet-laravel``

* Entrer dans le dossier du projet

``cd projet-laravel``

#### Installer vite dans Laravel

Laravel 10.x possède maintenant de base les dependancies pour pouvoir installer facilement vite.js présents dans package.lock.json.
Nous pouvons donc facilement installer les packages utilisant node.js.

``npm i``

#### Ajouter le plugin VueJs pour Vite

``npm add -D @vitejs/plugin-vue ``

#### Modifier vite.config.js et rajouter VueJs



```js 
// vite.config.js
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
>import vue from '@vitejs/plugin-vue';

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.css', 'resources/js/app.js'],
            refresh: true,
        }),
>        vue({
>            template: {
>                transformAssetUrls: {
>                    base: null,
>                    includeAbsolute: false,
>                },
>            },
>        }),
>    ],
>    resolve: {
>        alias: {
>            vue: 'vue/dist/vue.esm-bundler.js',
>        },
>    }, 
});
```
#### Ajouter le plugin vue-router pour mieux gérer les routes front grace à VueJS

```
npm install vue-router@4
```

#### Setup de VueJS

```js 
// app.js
// On importe vue et notre router vue dans notre app.js et on monte notre app dans #app
import './bootstrap';
>import { createApp } from 'vue';
>import  App  from './App.vue';
>import router from './router';

>const app = createApp(App);
>app.use(router);
>app.mount("#app");
```

```php

// welcome.blade.php 
// On crée notre #app dans notre view blade sur la route par défaut

...
<body>
  <div id="app">
  </div>
</body>
...
```

```js
// ressources/js/router/index.js
import { createRouter, createWebHistory } from 'vue-router'
import { HomePage } from '../components'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // routes
  ] 
})

export default router
```

Notre environnement est maintenant prêt pour pouvoir développer avec VueJS dans un environnement Laravel avec le bundler Vite.

### Intégrer la DB dans VueJS

* Modifier le .env et connecter la DB
* Créer une route api via api.php et qui va get les donneés
  
```php
  // Exemple api.php
Route::get('/users', function () {
    return new UserCollection(User::all());
});

url pour fetch les données: "../public/api/users"
```

* Créer un store pour fetch et get les données voulues
```js
// store/users.js
import { ref } from "vue";
const users = ref(null);

const fetchUsers = async () =>  {

    const response = await fetch("http://localhost/projet_web/laravel_vue_vite/public/api/users");
    return response.json();

};

const getUsers = async () => {
    users.value = await fetchUsers();
};
getUsers();

export { users };
```

* Importer les données dans la vue voulue
```js
//Home.vue
<template>
  <div>
    <h1>Home</h1>
      <div v-for="user in users.data" :key="user.id">
        {{ user.name }}
      </div>
  </div>    
</template>

<script setup>
>> import { users } from '../endpoints/users/users.js';


</script>

<style lang="scss" scoped>

</style>
```









