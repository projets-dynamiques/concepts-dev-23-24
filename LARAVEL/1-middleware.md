# Les Middleware : 

## Définition : 

Le middleware, en termes simples, agit comme un gardien entre la demande d'un utilisateur et la réponse fournie par l'application. Il permet d'effectuer certaines opérations avant que la demande n'atteigne la partie principale de l'application. C'est comme un filtre intelligent qui peut faire des vérifications ou des ajustements spécifiques avant que l'application ne réponde à la demande de l'utilisateur.

## L'utilité du Middleware : 

Le middleware est utile, parce qu'il sert de filtre. Il permet de réaliser des vérifications et des ajustements spécifiques avant que la demande n'atteigne la partie principale de l'application. En résumé, le middleware contribue à rendre le processus de traitement des demandes plus sécurisé, flexible et organisé.

## Son fonctionnement : 

![Middlware représentation](https://miro.medium.com/v2/resize:fit:945/1*RgPEcCE3mHSGR-fS5lXTCQ.png)

Lorsqu'une requête arrive dans une application Laravel, elle suit un chemin organisé appelé pipeline. Imaginez-le comme un parcours que la demande traverse. À chaque étape de ce parcours, il y a une couche appelée middleware qui peut faire des vérifications ou des modifications spécifiques sur la requête.

![Pipeline représentation](https://lh3.googleusercontent.com/-vpiFPbBHC-M/Vwowg1thjhI/AAAAAAAAAhk/_9vzMNvh62o/image4.png?imgmax=800)

Le plus important à retenir, c'est que chaque middleware agit comme un gardien qui examine la requête, fait quelque chose avec elle (comme vérifier l'authentification ou la valider), puis la passe à la couche suivante. C'est un peu comme une chaîne de contrôle de sécurité où chaque garde a un rôle spécifique.

Une fois que la demande a franchi toutes les couches de middleware, elle atteint la logique principale de l'application pour être traitée. Ensuite, lorsque l'application génère une réponse, elle repasse à travers ces couches de middleware dans l'ordre inverse avant d'être renvoyée au client. Cela garantit que toutes les couches ont la possibilité de faire des ajustements finaux à la réponse.

En résumé, le pipeline et les middleware sont un moyen organisé pour que l'application examine et traite les demandes, ajoutant des couches de sécurité et de personnalisation au processus

## Caractéristiques clés du Middleware dans Laravel : 

1. **Personnalisation** : Laravel offre la possibilité de créer des middleware personnalisés adaptés aux besoins spécifiques de l'application. Ces middleware peuvent être utilisés pour effectuer des tâches telles que l'authentification, la validation des données, la gestion des sessions, etc.
2. **Global Middleware** : Certains middleware sont enregistrés de manière globale, ce qui signifie qu'ils sont appliqués à chaque requête entrante sans nécessiter une configuration spécifique au sein des routes.
3. **Exécution Conditionnelle** : Les middleware peuvent être configurés pour s'exécuter conditionnellement en fonction de certaines conditions. Par exemple, un middleware d'authentification peut être configuré pour s'exécuter uniquement si un utilisateur n'est pas déjà authentifié.
4. **Manipulation de la Requête et de la Réponse** : Les middleware peuvent accéder à l'objet de requête et à l'objet de réponse, leur permettant de modifier ces objets en fonction des besoins de l'application.
5. **Terminaison de la Requête** : Certains middleware peuvent terminer le traitement de la requête prématurément, empêchant ainsi les couches de middleware suivantes d'être exécutées. Cela peut être utile pour des opérations comme la redirection avant d'atteindre la logique de l'application.
6. **Middleware de Réécriture d'URL** : Modifie l'URL de la requête avant le traitement pour rediriger ou filtrer les requêtes.

## Quelques exemples de Middleware fréquents avec le code : 

1. **Middleware d'Authentification :** Vérifie si l'utilisateur est authentifié avant de permettre l'accès à certaines parties de l'application. 
 Dans le fichier Authenticate : 

    ```php
    // app/Http/Middleware/Authenticate.php
    namespace App\Http\Middleware;
    use Closure;
    use Illuminate\Support\Facades\Auth;
    class Authenticate
    {
        public function handle($request, Closure $next)
        {
            if (Auth::guest()) {
                return redirect('/login');
            }
            return $next($request);
        }
    }
    ```
    Dans le fichier kernel : 

    ```php
    // App/Http/Kernel.php
    protected $middleware = [
        // ...
        \App\Http\Middleware\Authenticate::class,
    ];
    ```

    Dans le routeur : 

    ```php
    // routes/web.php
    use App\Http\Controllers\YourController;
    use App\Http\Middleware\Authenticate;
    Route::middleware([Authenticate::class])->group(function () {
    // Les routes ici ne seront accessibles qu'aux utilisateurs authentifiés
    Route::get('/dashboard', [YourController::class, 'dashboard']);
});
    ```

Si l'utilisateur n'est pas authentifié, il est redirigé vers la page de connexion.

2. **Middleware de Vérification d'Autorisation** : Vérifie si l'utilisateur a les autorisations nécessaires pour effectuer certaines actions
    ```php
    // app/Http/Middleware/CheckAuthorization.php
    namespace App\Http\Middleware;
    use Closure;
    class CheckAuthorization
    {
        public function handle($request, Closure $next)
        {
            // Vérifie si l'utilisateur a les autorisations nécessaires
            if (!auth()->user()->hasPermission('access_admin_panel')) {
                return redirect('/')->with('error', 'Vous n\'avez pas les autorisations nécessaires.');
            }
            return $next($request);
        }
    }
    ```
    Dans le routeur : 
    ```php
    use App\Http\Controllers\YourController;
    use App\Http\Middleware\CheckAuthorization;
    Route::middleware([CheckAuthorization::class])->group(function () {
        // Les routes ici ne seront accessibles qu'aux utilisateurs ayant les autorisations nécessaires
        Route::get('/admin-panel', [YourController::class, 'adminPanel']);});
    ```
Si l'utilisateur n'a pas les autorisations, le middleware empêche un utilisateur non autorisé d'accéder à une page d'administration.

3. **Middleware de Localisation** : Modifie la langue de l'application en fonction des préférences de l'utilisateur.

    ```php 
    // app/Http/Middleware/SetLocale.phps
    namespace App\Http\Middleware;
    use Closure;
    use Illuminate\Http\Request;
    class SetLocale
    {
        public function handle(Request $request, Closure $next)
        {
            // Récupère la langue préférée de l'utilisateur depuis ses préférences ou utilise la langue par défaut
            $locale = $request->hasHeader('Accept-Language') ? $request->header('Accept-Language') : config('app.locale');
            // Définit la langue de l'application en fonction de la préférence de l'utilisateur
            app()->setLocale($locale);
            return $next($request);
        }
    }
    ```
## Liste d'exemples de Middleware courants : 
1. **Middleware de Journalisation** : Enregistre des informations sur les requêtes entrantes, utile pour la débogage et la surveillance.
2. **Middleware de Compression** : Compresse la réponse avant de l'envoyer au client pour économiser la bande passante.
3. **Middleware de Protection CSRF (Cross-Site Request Forgery)** : Ajoute une couche de sécurité en vérifiant si les demandes POST, PUT, PATCH, et DELETE contiennent un jeton CSRF valide.
4. **Middleware de Gestion des Sessions** : Gère le cycle de vie des sessions utilisateur, permettant de stocker des données entre les requêtes.
5. **Middleware de Cache** : Gère la mise en cache pour améliorer les performances en stockant temporairement les résultats de certaines opérations.
6. **Middleware de Maintenance** : Redirige toutes les requêtes vers une page de maintenance pendant une période définie.
7. **Middleware de Gestion des erreurs** : Capture et gère les erreurs lors du traitement des requêtes.

## Création de Middleware  : 

### Commande php artisan pour les Middleware  : 

1. Pour créer un nouveau Middleware 
   ```php 
    php artisan make:middleware NomDuMiddleware
   ```
Cette commande permet de créer un nouveau middleware avec le nom spécifié. Le middleware sera créé dans le répertoire app/Http/Middleware.

2. Lister les Middleware enregistrés
    ```php 
    php artisan route:list
   ```  
Cette commande liste toutes les routes enregistrées dans l'application, et elle inclut également les middleware associés à chaque route.

### Un Middleware basique : 

Ce middleware vérifie si l'application est en mode maintenance en utilisant la méthode *isDownForMaintenance()* fournie par Laravel. 
Si l'application est en mode maintenance, il renvoie une réponse avec le message "Mode maintenance" et un code HTTP 503 (Service Unavailable), ce qui signifie que le service n'est pas disponible temporairement. Sinon, il laisse passer la requête vers la prochaine étape, représentée par ```$next($request)```.

Pour utiliser ce code comme un middleware global dans une application Laravel, nous devons : créer une classe Middleware et enregistrer ce middleware dans le noyau de l'application.

***Pour un Middleware Global qui affiche une page personnalisée***

Dans le fichier MaintenanceModeMiddleware :
```php
namespace App\Http\Middleware;
use Closure;
class MaintenanceModeMiddleware
    {
        public function handle($request, Closure $next)
        {
            if (app()->isDownForMaintenance()) {
            return response()->view('maintenance');
        }
            return $next($request);;
        }
    }
```
Dans le fichier kernel : 
```php
    protected $middleware = [
        // Autres middlewares...
        \App\Http\Middleware\MaintenanceModeMiddleware::class,
    ];
```
