# Gestion des erreurs et des exceptions dans Laravel

## Sommaire
- [Gestion des erreurs et des exceptions dans Laravel](#gestion-des-erreurs-et-des-exceptions-dans-laravel)
  - [Sommaire](#sommaire)
  - [Gestion globale des exceptions :](#gestion-globale-des-exceptions-)
  - [Les types d'exceptions courantes :](#les-types-dexceptions-courantes-)
  - [Gestion des erreurs AJAX :](#gestion-des-erreurs-ajax-)
  - [Personnalisation des pages d'erreur :](#personnalisation-des-pages-derreur-)
  - [Gestion des erreurs de validation :](#gestion-des-erreurs-de-validation-)
  - [Enregistrement des erreurs :](#enregistrement-des-erreurs-)


##Introduction

Laravel offre plusieurs mécanismes pour gérer les erreurs et les exceptions, il propose également des moyens de personnaliser ces comportements.

## Gestion globale des exceptions :

- **Fichier `Handler.php` dans le dossier `App\Exceptions` :**
  Dans Laravel, ce fichier joue un rôle central dans la gestion des problèmes qui surviennent pendant l'exécution de votre application. Il contient deux méthodes importantes : `report` et `render`.
  - La méthode `report` est utilisée pour enregistrer les erreurs survenues dans l'application, ce qui peut être utile pour garder une trace des erreurs dans un fichier journal (log) ou pour notifier les administrateurs de système.
  Exemple:
    ```php
    // Enregistrement d'une exception dans le journal (log)
    public function report(Exception $exception)
    {
        if ($exception instanceof CustomException) {
            Log::error('Custom Exception: ' . $exception->getMessage());
        }

        parent::report($exception);
    }
    ```
  - La méthode `render` intervient lorsqu'une exception est déclenchée. Elle permet de transformer cette exception en une réponse HTTP adaptée à l'utilisateur. 
  Par exemple, si une page est introuvable, Laravel peut renvoyer un message d'erreur 404 au lieu d'une page vide.
    ```php
    // Personnalisation de la réponse HTTP pour une exception spécifique
    public function render($request, Exception $exception)
    {
        if ($exception instanceof NotFoundHttpException) {
            return response()->view('errors.404', [], 404);
        }

        return parent::render($request, $exception);
    }
    ```
## Les types d'exceptions courantes :

- **HttpException :**
  Lorsque votre application Laravel rencontre des erreurs HTTP telles que 404 (introuvable) ou 500 (erreur interne du serveur), elle utilise la classe `HttpException` pour les gérer. Vous pouvez personnaliser la façon dont ces erreurs sont gérées (voir [la personnalisation des pages d'erreur](#personnalisation-des-pages-derreur-)).

- **ModelNotFoundException :**
  Si votre application ne trouve pas un modèle spécifique dans la base de données, Laravel lance une `ModelNotFoundException`. Vous pouvez personnaliser la façon dont ces exceptions sont gérées dans le fichier `Handler.php`.

  Par exemple, si vous souhaitez rediriger l'utilisateur vers une page spécifique lorsque le modèle demandé n'est pas trouvé, vous pouvez ajouter une logique de redirection dans la méthode `render` de `Handler.php`.
  ```php
  public function render($request, Exception $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            // Redirection de l'utilisateur vers une page spécifique
            return redirect()->route('page.introuvable');
        }

        return parent::render($request, $exception);
    }
    ```
## Gestion des erreurs AJAX :

- **Gestion des erreurs AJAX :**
  Lorsque des erreurs surviennent dans les requêtes AJAX de votre application Laravel, elles sont généralement gérées en renvoyant des réponses JSON. Vous pouvez personnaliser ces réponses JSON dans la méthode `render` du fichier `Handler.php` en vérifiant si la requête est de type AJAX à l'aide de `$request->ajax()`.

  Par exemple, si une erreur survient lors d'une requête AJAX, vous pouvez renvoyer une réponse JSON avec un message d'erreur approprié.
  ```php
  public function render($request, Exception $exception)
    {
        if ($request->ajax() || $request->wantsJson()) {
            // Vérifie si la requête est de type AJAX
            return new JsonResponse(['error' => 'Erreur lors de la requête AJAX'], 422);
        }

        return parent::render($request, $exception);
    }
    ```

## Personnalisation des pages d'erreur :

- **Personnalisation des pages d'erreur :**
  Vous avez la possibilité de personnaliser les pages d'erreur de votre application en modifiant les fichiers situés dans le répertoire `resources\views\errors\` ou en redéfinissant les méthodes correspondantes dans le fichier `Handler.php`.

  Par exemple, si vous souhaitez personnaliser la page d'erreur 500 pour afficher un message spécifique à votre application, vous pouvez modifier la vue `500.blade.php` dans le dossier `resources\views\errors\`.

- **Personnalisation des réponses d'erreur :**
  En plus de personnaliser les pages d'erreur, vous pouvez également personnaliser les réponses HTTP renvoyées pour différents types d'exceptions. Cela se fait en modifiant les méthodes `render` dans `Handler.php`.

  Par exemple, si vous souhaitez personnaliser la réponse HTTP pour une erreur spécifique comme le code 403 (interdiction d'accès), vous pouvez le faire dans la méthode `render` de `Handler.php`.
  ```php
  public function render($request, Exception $exception)
    {
        if ($exception instanceof AuthorizationException) {
            // Personnalisation de la réponse HTTP pour une erreur 403
            return response()->view('errors.403', [], 403);
        }

        return parent::render($request, $exception);
    }
    ```

## Gestion des erreurs de validation :

- **Erreurs de validation :**
  Les erreurs de validation sont gérées automatiquement par Laravel lors de l'utilisation de la classe `Validator`. Vous pouvez personnaliser les messages d'erreur affichés aux utilisateurs dans le fichier de langues situé dans `resources\lang\` en fonction de la langue de votre application.

  Par exemple, si vous souhaitez modifier le message d'erreur pour un champ de formulaire spécifique, vous pouvez le faire en modifiant le fichier `validation.php` dans le répertoire de langue correspondant.
  ```php
    // Dans resources\lang\fr\validation.php
    return [
        'email' => 'L\'adresse e-mail saisie n\'est pas valide.',
    ];
    ```
- **Personnalisation des règles de validation :**
  En plus de personnaliser les messages d'erreur, vous pouvez également créer vos propres règles de validation personnalisées en étendant la classe `Illuminate\Validation\Rule` et en les utilisant dans vos validateurs.

  Par exemple, si vous souhaitez créer une règle de validation personnalisée pour vérifier qu'un champ est un numéro de téléphone valide, vous pouvez le faire en créant une nouvelle règle dans votre classe de validation.
  ```php
    // Création d'une nouvelle règle de validation pour vérifier un numéro de téléphone
    public function rules()
    {
        return [
            'telephone' => [
                'required',
                Rule::phone()->country(['FR', 'BE', 'CA']) // Validation du numéro de téléphone pour certains pays
            ],
        ];
    }
    ```
## Enregistrement des erreurs :

- **Logging :**
  Laravel propose une solution de logging intégrée qui vous permet d'enregistrer les erreurs et les événements importants de votre application dans différents canaux de log. Vous pouvez configurer ces canaux dans le fichier `config\logging.php`.

  Par exemple, si vous souhaitez enregistrer les erreurs dans un fichier journal dédié, vous pouvez configurer le canal `single` dans `logging.php` pour enregistrer les erreurs dans un fichier spécifique.
    ```php
    // Dans le fichier config/logging.php
    return [
        'default' => env('LOG_CHANNEL', 'stack'),
        'channels' => [
            'stack' => [
                'driver' => 'stack',
                'channels' => ['single'],
            ],
            'single' => [
                'driver' => 'single',
                'path' => storage_path('logs/laravel.log'),
                'level' => 'error',
            ],
            // Ajoutez d'autres canaux de journalisation si nécessaire...
        ],
    ];
    ```
- **Notifications :**
  Laravel offre également la possibilité d'envoyer des notifications pour des événements d'erreurs spécifiques. Vous pouvez configurer ces notifications dans le fichier `App\Exceptions\Handler.php`.

  Par exemple, si vous souhaitez être informé par e-mail lorsqu'une erreur critique se produit dans votre application, vous pouvez ajouter une logique de notification dans la méthode `report` de `Handler.php`.
    ```php
    public function report(Throwable $exception)
    {
        if ($this->shouldReport($exception)) {
            // Envoyer une notification par e-mail
            Mail::raw('Une erreur critique s\'est produite dans l\'application.', function ($message) {
                $message->to('destinataire@example.com')
                        ->subject('Erreur critique dans l\'application');
            });
        }
        parent::report($exception);
    }
    ```
En résumé, Laravel offre une grande flexibilité pour gérer les erreurs et les exceptions, vous permettant de personnaliser ces comportements selon les besoins de votre application. La documentation officielle de Laravel est une excellente ressource pour obtenir des informations détaillées sur ces sujets.
