#**Guide sur les concepts de Service Container et Service Providers**

## Sommaire

## Service Container


### Introduction
Le service container de Laravel est un outil puissant pour gérer les dépendances de classe et effectuer l'injection de dépendances. Il agit comme un conteneur d'instances d'objets et de dépendances, offrant une manière flexible et dynamique de gérer les composants de votre application.

### Fonctionnement du Service Container
Le service container est capable de résoudre automatiquement les dépendances des classes, facilitant ainsi le développement et la maintenance du code. Les dépendances sont injectées dans les classes via le constructeur ou des méthodes "setter", favorisant une conception modulaire et testable. De plus, le conteneur peut résoudre automatiquement les classes sans nécessiter de configuration explicite.

### Utilisation du Service Container
Le service container est utile pour gérer efficacement et de manière flexible les dépendances des classes. Il est également utile pour injecter automatiquement les dépendances dans les classes sans intervention manuelle.

### Liaison
#### Principes de base de la liaison

La liaison dans le service container consiste à associer des classes ou des interfaces à des implémentations spécifiques. Cette liaison permet au service container de comprendre quelles classes doivent être instanciées lorsqu'une certaine classe ou interface est demandée.

#### Liaison d'Interfaces à des Implémentations

Permet d'associer une interface à une implémentation spécifique :

```php
$this->app->bind(Interface::class, Implementation::class);
```

#### Liaison Contextuelle
La liaison contextuelle permet d'injecter différentes implémentations en fonction du contexte d'utilisation d'une classe. Cela peut être utile lorsque vous avez besoin de comportements différents pour une même interface en fonction de certains critères. Par exemple, selon le type d'utilisateur ou d'environnement :

```php
$this->app->when(Class::class)
          ->needs(Interface::class)
          ->give(Implementation::class);
```

#### Liaison de Primitives
Permet d'associer des valeurs primitives à des classes :

```php
$this->app->when(Class::class)
          ->needs('$variable')
          ->give($value);
```

### Tagging
La balise permet de regrouper des liaisons sous une même étiquette pour une résolution ultérieure.

```php
$this->app->tag([Class1::class, Class2::class], 'tag');
```

### Extension des Liaisons
Permet de modifier les services résolus pour ajouter des fonctionnalités supplémentaires ou configurer les services.

```php
$this->app->extend(Class::class, function ($instance, $app) {
    // Modification du service...
    return $instance;
});
```

### Résolution
La résolution consiste à obtenir une instance de classe à partir du conteneur.

#### Utilisation de la Méthode Make
Permet de résoudre une instance de classe à partir du conteneur.

```php
$instance = $this->app->make(Class::class);
```

#### Injection Automatique
Le service container peut injecter automatiquement les dépendances dans les classes résolues. Cela signifie que vous n'avez pas besoin de spécifier explicitement les dépendances, elles seront automatiquement résolues et injectées par le conteneur.

```php
class ExampleController extends Controller
{
    public function __construct(Class $class)
    {
        // Injection automatique de dépendances...
    }
}
```

#### Invocation de Méthode et Injection
Permet d'invoquer une méthode sur une instance tout en injectant automatiquement les dépendances.

```php
$report = $this->app->call([$instance, 'method']);
```

### Événements de Conteneur
Les événements de conteneur permettent de surveiller les interactions du conteneur.

```php
Copy code
$this->app->resolving(function ($object, $app) {
    // Traitement lors de la résolution d'une instance...
});
```

### PSR-11
Le PSR-11 est une spécification pour les conteneurs de services en PHP. Le service container de Laravel implémente le PSR-11, ce qui signifie qu'il respecte cette spécification et peut être utilisé comme un conteneur de services conforme au PSR-11.

```php
use App\Services\Transistor;
use Psr\Container\ContainerInterface;
 
Route::get('/', function (ContainerInterface $container) {
    $service = $container->get(Transistor::class);
 
    // ...
});
```

---

### Conclusion

Le Service Container de Laravel est un pilier central dans la gestion des dépendances et la configuration des services de votre application. En automatisant la résolution des dépendances et en permettant l'injection d'instances de classe de manière transparente, il simplifie le développement et améliore la maintenabilité du code.

Il joue également un rôle essentiel dans l'architecture de Laravel, travaillant de concert avec les Service Providers pour enregistrer des liaisons, configurer des services et étendre les fonctionnalités de l'application.

En résumé, le Service Container est un outil puissant qui offre une manière élégante et efficace de gérer les dépendances et de configurer les services de votre application dans Laravel.

Dans la section suivante, nous explorerons le rôle des **Service Providers** et leur interaction avec le **Service Container** pour étendre les fonctionnalités de votre application.

## Service Providers

### Introduction

Les service providers sont essentiels dans Laravel pour configurer et initialiser divers composants de l'application. Ils permettent d'enregistrer des liaisons dans le conteneur de services, des écouteurs d'événements, des compositeurs de vues, et plus encore.

### Écriture de Service Providers

Tous les service providers étendent la classe **`Illuminate\Support\ServiceProvider`**. Ils contiennent généralement une méthode **`register`** pour enregistrer des liaisons et une méthode **`boot`** pour des tâches de configuration supplémentaires :

```php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Riak\Connection;
use Illuminate\Contracts\Foundation\Application;

class RiakServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Connection::class, function (Application $app) {
            return new Connection(config('riak'));
        });
    }
}
```

### Enregistrement des Providers

Tous les service providers sont répertoriés dans le fichier de configuration **`config/app.php`**. Ils sont ajoutés au tableau **`providers`**. Les providers différés peuvent améliorer les performances en retardant leur chargement jusqu'à ce que leurs services soient réellement nécessaires :

```php
'providers' => [
    // Providers Laravel par défaut
    App\Providers\RiakServiceProvider::class,
],
```

### Providers différés
Les providers différés retardent leur chargement jusqu'à ce que leurs services soient demandés. Ils implémentent l'interface **`Illuminate\Contracts\Support\DeferrableProvider`** et définissent une méthode **`provides`** pour lister les services qu'ils fournissent.

```php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Riak\Connection;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\DeferrableProvider;

class RiakServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register()
    {
        $this->app->singleton(Connection::class, function (Application $app) {
            return new Connection($app['config']['riak']);
        });
    }

    public function provides()
    {
        return [Connection::class];
    }
}
```

---

### Exemple d'emploi de Service Container

Supposons que nous ayons une classe UserService qui dépend d'une autre classe EmailService. Nous allons utiliser le service container pour gérer ces dépendances :

#### Classe EmailService

```php
namespace App\Services;

class EmailService
{
    public function send($email, $message)
    {
        // Code pour envoyer un e-mail
        return "E-mail envoyé à $email : $message";
    }
}
```

#### Classe UserService

```php
namespace App\Services;

class UserService
{
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function registerUser($email)
    {
        // Logique pour l'inscription de l'utilisateur
        $message = "Bienvenue sur notre site!";
        return $this->emailService->send($email, $message);
    }
}
```

#### Utilisation du Service Container

```php
use Illuminate\Support\Facades\App;

// Enregistrer la liaison dans le service container
App::bind('emailService', function ($app) {
    return new \App\Services\EmailService();
});

// Résoudre UserService à partir du service container
$userService = App::make(\App\Services\UserService::class);

// Utiliser le service UserService
$result = $userService->registerUser('test@example.com');

echo $result;
```

Dans cet exemple, nous avons utilisé le service container pour enregistrer la liaison entre l'interface EmailService et son implémentation concrète. Ensuite, nous avons résolu la classe UserService à partir du service container, et le service container a automatiquement injecté l'instance de EmailService lors de la résolution de UserService.

Enfin, nous avons appelé la méthode registerUser sur l'instance de UserService qui, à son tour, a utilisé l'instance de EmailService pour envoyer un e-mail à l'utilisateur.