
# Guide Complet sur la Gestion des Sessions HTTP dans Laravel

## Introduction
Les sessions dans Laravel servent à garder des informations sur les utilisateurs pendant qu'ils naviguent sur votre site web. Même si le web fonctionne sans garder en mémoire l'état des interactions (chaque requête est indépendante), les sessions permettent de "se souvenir" des utilisateurs entre chaque action qu'ils font. Laravel facilite l'utilisation des sessions en offrant une même manière de les gérer, que vos données soient stockées dans des fichiers, en base de données, ou ailleurs.

## Configuration
Configurez les sessions dans `config/session.php`. Laravel propose plusieurs pilotes de session :
- `file`: stockage dans `storage/framework/sessions`.
- `cookie`: stockage dans des cookies cryptés.
- `database`: utilisation d'une base de données.
- `memcached` / `redis`: stockage dans ces systèmes de cache.
- `dynamodb`: stockage dans AWS DynamoDB.
- `array`: stockage dans un tableau PHP (sans persistance).

### Exemple de Configuration
```php
'driver' => env('SESSION_DRIVER', 'file'),
```

## Conditions Préalables du Pilote
### Base de Données
Pour le pilote `database`, créez une table pour stocker les sessions :
```php
Schema::create('sessions', function (Blueprint $table) {
    $table->string('id')->primary();
    $table->foreignId('user_id')->nullable()->index();
    $table->string('ip_address', 45)->nullable();
    $table->text('user_agent')->nullable();
    $table->text('payload');
    $table->integer('last_activity')->index();
});
```
Utilisez la commande `php artisan session:table` pour générer cette migration.

### Redis
Pour Redis, installez l'extension PHP `PhpRedis` ou le package `predis/predis`.

## Interagir avec la Session
### Récupération des Données
Utilisez `get` pour récupérer des données :
```php
$value = $request->session()->get('key', 'default');
```

### Stocker des Données
Stockez des données avec `put` :
```php
$request->session()->put('key', 'value');
```
### Pousser vers les valeurs de session du tableau
La méthode `push` peut être utilisée pour insérer une nouvelle valeur sur une valeur de session qui est un tableau.
```php
$request->session()->push('user.teams', 'developers');
```

### Récupération et suppression d'un élément
La méthode `pull` récupérera et supprimera un élément de la session en une seule instruction :
```php
$value = $request->session()->pull('key', 'default');
```

### Récupération d'une partie des données de session
Les méthodes `only` et `except` peuvent être utilisées pour récupérer un sous-ensemble des données de session :
```php
$data = $request->session()->only(['username', 'email']);
 
$data = $request->session()->except(['username', 'email']);
```

### Déterminer si un élément existe. La méthode renvoie `true` ou `false`
```php
if ($request->session()->has('users')) {
    // ...
}
```

### Pour déterminer si un élément est présent dans la session, même si sa valeur est `null`, vous pouvez utiliser la existsméthode :
```php 
if ($request->session()->exists('users')) {
    // ...
}
```

## Suppression de Données
La méthode `forget` permet de supprimer une donnée spécifique de la session, tandis que `flush` efface toutes les données.

```php
// Oublier une clé spécifique...
$request->session()->forget('name');

// Oublier plusieurs clés...
$request->session()->forget(['name', 'status']);

// Vider complètement la session...
$request->session()->flush();
```

## Régénération de l'ID de Session
Régénérer l'ID de session est une pratique courante pour prévenir les attaques de fixation de session.

### Exemples de Code
```php
// Régénérer l'ID de session...
$request->session()->regenerate();

// Régénérer l'ID de session et supprimer toutes les données...
$request->session()->invalidate();
```

## Blocage de Session
Le blocage de session est utile pour synchroniser les requêtes utilisant la même session.

### Exemple d'Utilisation
```php
Route::post('/profile', function () {
    // ...
})->block($lockSeconds = 10, $waitSeconds = 10);

Route::post('/order', function () {
    // ...
})->block($lockSeconds = 10, $waitSeconds = 10);
```

## Données Flash
Stockez des données temporaires avec `flash` afin de les utilisés dans le prochaine demande.
Imaginous que l'on a un formulaire pour ajouter un utilisateur. Voici ce qu'il faudrait mettre dans le controller pour utiliser flash afin de confirmé l'ajout.
```php
public function deleteUser(Request $request, $userId = null)
{
    public function deleteUser($userId = null)
    {
        // Simuler la logique de suppression d'un utilisateur
    
        // Flasher un message dans la session avant de rediriger
        session()->flash('message', "L'utilisateur a bien été supprimé !");
    
        // Rediriger vers une page, par exemple la page d'accueil
        return redirect('/');
    }
}
```
### Routes pour le message de session
```php
// Route simulée
Route::get('/delete_user/{userId}', [SessionController::class, 'deleteUser']);
```

### Affichage dans la vue 
```php
// Affichage dans le DOM

    @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
    @endif
</div>

// Affichage Dans un toast

<script>
        document.addEventListener('DOMContentLoaded', function() {
            @if (session('message'))
                var message = "{{ session('message') }}";
                M.toast({html: message});
            @endif
        });
    </script>
```

### Résultat du message flash

![Image de confirmation](./imgBenja/captureBenja.png)